'use strict';

let utilsService = {
    daysInAMonth: daysInAMonth,
    daysInCurrentMonth: daysInCurrentMonth,
    getLocalDateTime: getLocalDateTime,
    roundTo100ths: roundToHundredths
};

module.exports = utilsService;

/* Private functions */

function daysInAMonth(year, month) {
    return new Date(year, month, 0).getDate();
}

function daysInCurrentMonth() {
    let now = new Date();

    now.toLocaleDateString
    return daysInAMonth(now.getFullYear(), now.getMonth());
}

function getLocalDateTime() {
    let now = new Date();
    return now.toLocaleDateString() + ' ' + now.toLocaleTimeString();
}

function roundToHundredths(value) {
    return Math.round(value * 100) / 100
}
