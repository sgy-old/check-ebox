'use strict';

const Promise = require('bluebird');

const nightmareService = {
    getData: getData
};    

module.exports = nightmareService;

/* Private fonctions */

function getData() {
    return new Promise((resolve, reject) => {
        const config = require('../conf/config.json');
        const scraperConfig = require('../conf/scraper.json');

        console.log('Logging into page...');

        let Nightmare = require('nightmare');
        let nightmare = Nightmare(scraperConfig);

        nightmare
        .goto(config.provider.url)
        .type('input[name=usrname]', config.provider.user)
        .type('input[name=pwd]', config.provider.password)
        .click('a#btnLogin')
        .wait(scraperConfig.pageWait)
        .evaluate(scrapePage)
        .run((err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })
        .end();
    });
}

function scrapePage() {
    let bandwidth = { };
    let bandwidthElement = document.querySelector('span.text_summary3');

    if (bandwidthElement) {
        let bandwidthRaw = bandwidthElement.innerHTML;
        let regx = /(\d{1,3}(\.\d{1,3})?)\s+Go\s+\/(?:\s*)(\d{1,4})\s+Go/g;
        let match = regx.exec(bandwidthRaw);
        let consumption = parseFloat(match[1]);
        let limit = parseInt(match[3]);

        bandwidth = {
            consumption: consumption,
            limit: limit
        }
    }

    let remaining = undefined;
    let remainingElement = document.querySelector('span.text_summary2');

    if (remainingElement) {
        let remainingRaw = remainingElement.innerHTML;
        let regx = /(\d{1,2})\s+jour(?:s)?\srestant/g;
        let match = regx.exec(remainingRaw);
        remaining = parseInt(match[1]);
    }

    let amount = 0;
    let amountElement = document.querySelector('div.text_amount');

    if (amountElement) {
        let amountRaw = amountElement.innerHTML;
        let regx = /(\d{1,3}\.\d{2})\s\$/g;
        let match = regx.exec(amountRaw);
        amount = parseFloat(match[1]);
    }

    return {
        bandwidth: bandwidth,
        remaining: remaining,
        amount: amount
    };
}
